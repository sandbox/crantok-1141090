Defines a Notifications (v4) subscription type for content tagged with a given
taxonomy term posted to a given Organic group. Currently at a very early stage,
with work required to make it consistent and generically useful, but it works.
